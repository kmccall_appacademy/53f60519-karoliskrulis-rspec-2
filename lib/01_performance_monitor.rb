
def measure(n = 0, &b)
  start_time = Time.now
  if n == 0
    b.call
  else
    n.times { b.call }
  end
  (Time.now - start_time) / (n == 0 ? 1 : n)
end
