
def reverser
  if block_given?
    str = yield.split(' ')
    str.map { |word| word.reverse }.join(' ')
  end
end

def adder(num = 1)
  num + yield
end

def repeater(n = 0)
  if n == 0
    yield
  else
    n.times { yield }
  end
end
